CREATE DATABASE UniversityDB;

USE UniversityDB

create table UniversityDB.dbo.FacultyTable(
FacultyId int identity(1,1),
FacultyName varchar(500)
)

SELECT * FROM dbo.FacultyTable

insert into dbo.FacultyTable values
('Support')

create table UniversityDB.dbo.StudentTable(
StudentId int identity(1,1),
StudentName varchar(500),
FacultyName varchar(500),
DateOfMatriculation date,
PhotoFileName varchar(500)
)


SELECT * FROM UniversityDB.dbo.StudentTable

insert into UniversityDB.dbo.StudentTable values
('Marius','AC','2018-10-01','defaultPhoto.png')